from email.mime import image
from detect import detect
import argparse
import numpy as np
import cv2
import streamlit as st

with st.sidebar:
    st.title('Nhom 2')
    st.title('Thanh vien nhom: ')
    st.subheader("Hoang Dinh Quang - 18521294")
    st.subheader("Hoang Dinh Quang - 18521294")
    st.subheader("Hoang Dinh Quang - 18521294")
    st.title("Giao vien huong dan: TS. Do Trong Hop")

st.title('Ten gi do thi tu chon')
with st.form("my_form"):
   st.write("Inside the form")
   path_image = st.text_input("Path_image")
   # Every form must have a submit button.
   submitted = st.form_submit_button("Send")
   if submitted:
        with st.spinner('Wait for it...'):
                parser = argparse.ArgumentParser()
                parser.add_argument('--weights', nargs='+', type=str,
                                default='yolov7.pt', help='model.pt path(s)')
                # file/folder, 0 for webcam
                parser.add_argument('--source', type=str,
                                default='inference/images', help='source')
                parser.add_argument('--img-size', type=int, default=640,
                                help='inference size (pixels)')
                parser.add_argument('--conf-thres', type=float,
                                default=0.25, help='object confidence threshold')
                parser.add_argument('--iou-thres', type=float,  
                                default=0.45, help='IOU threshold for NMS')
                parser.add_argument('--device', default='',
                                help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
                parser.add_argument('--view-img', action='store_true',
                                help='display results')
                parser.add_argument('--save-txt', action='store_true',
                                help='save results to *.txt')
                parser.add_argument('--save-conf', action='store_true',
                                help='save confidences in --save-txt labels')
                parser.add_argument('--nosave', action='store_true',
                                help='do not save images/videos')
                parser.add_argument('--classes', nargs='+', type=int,
                                help='filter by class: --class 0, or --class 0 2 3')
                parser.add_argument('--agnostic-nms', action='store_true',
                                help='class-agnostic NMS')
                parser.add_argument('--augment', action='store_true',
                                help='augmented inference')
                parser.add_argument('--update', action='store_true',
                                help='update all models')
                parser.add_argument('--project', default='runs/detect',
                                help='save results to project/name')
                parser.add_argument('--name', default='exp',
                                help='save results to project/name')
                parser.add_argument('--exist-ok', action='store_true',
                                help='existing project/name ok, do not increment')
                parser.add_argument('--no-trace', action='store_true',
                                help='don`t trace model')
                opt = parser.parse_args()
                opt.source = path_image
                opt.weights = "weights/yolov7.pt"
                img_orgin = cv2.imread(path_image)
                img_orgin = cv2.cvtColor(img_orgin, cv2.COLOR_BGR2RGB)
                img_orgin = cv2.resize(img_orgin,(640,640))
                st.subheader('This is a input image')
                st.image(img_orgin)

                result,image_detect = detect(opt)
                image_detect = cv2.cvtColor(image_detect, cv2.COLOR_BGR2RGB)
                image_detect = cv2.resize(image_detect,(640,640))
                st.subheader('This is a image detected')
                st.image(image_detect)
                st.subheader('This is a text recognized')
                st.write(result)
                audio_file = open('gtts.wav', 'rb')
                audio_bytes = audio_file.read()
                st.subheader('This is a audio recognized')
                st.audio(audio_bytes)
        st.success('Done!')


# Convert the file to an opencv image.


# detect_ocr()
